#include <iostream>
#include <string>
#include <memory>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_pointer.hpp>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
	BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
	BOOST_STATIC_ASSERT(sizeof(int)==4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
    BOOST_STATIC_ASSERT(
                boost::is_base_of<Base, T>::value
                && "To nie jest typem pochodnym po Base" );
    static_assert(boost::is_base_of<Base, T>::value, "T nie jest typem pochodnym po Base");
}


template <typename T>
struct is_shared_ptr : public boost::false_type
{};

template <typename T>
struct is_shared_ptr<boost::shared_ptr<T> > : public boost::true_type
{};

template <typename T>
struct is_shared_ptr<std::shared_ptr<T> > : public boost::true_type
{};

template <typename T>
void CompatibileWithPointersAndSharedPtrs(T ptr)
{
    BOOST_STATIC_ASSERT(boost::is_pointer<T>::value
                        || is_shared_ptr<T>::value);

    std::cout << "Wyluskanie wskaznika: " << *ptr << std::endl;
}

int main()
{
    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[100];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
    works_with_base_and_derived(arg);

    int* ptr_int = &arr[0];

    CompatibileWithPointersAndSharedPtrs(ptr_int);

    boost::shared_ptr<std::string> ptr_str = boost::make_shared<std::string>("Text");

    CompatibileWithPointersAndSharedPtrs(ptr_str);

    std::shared_ptr<std::string> stdptr_str(new std::string("StdSharedPtr"));

    CompatibileWithPointersAndSharedPtrs(stdptr_str);
}
