#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>

using namespace std;

void foo(int x)
{
    cout << "foo(int: " << x << ")" << endl;
}

struct MyType
{
    int value;
    MyType(int v) : value(v) {}

    typedef void nested_type;
};

ostream& operator<<(ostream& out, const MyType& mt)
{
    out << mt.value;

    return out;
}

template <typename T>
typename boost::disable_if<boost::is_integral<T> >::type foo(T arg)
{
    cout << "foo(T: " << arg << ")" << endl;
}

//template <typename T>
//void foo(T arg, typename T::nested_type* ptr = 0)
//{
//    cout << "foo(T: " << arg << ")" << endl;
//}

//template <typename T>
//typename T::nested_type foo(T arg)
//{
//    cout << "foo(T: " << arg << ")" << endl;
//}


int main()
{
    foo(7);

    short sx = 7;
    foo(sx);

    double dx = 3.14;
    foo(dx);
}

