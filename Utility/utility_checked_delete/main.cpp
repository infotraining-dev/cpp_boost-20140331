#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>
#include <algorithm>
#include <boost/checked_delete.hpp>

class SomeClass;

SomeClass* create()
{
	return (SomeClass*)0;
}

void simple_test()
{
//    SomeClass* p = create();

//    delete p; // w najlepszym razie warning
}

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
	real_test();

    std::vector<ToBeDeleted*> vec_ptrs;

    vec_ptrs.push_back(new ToBeDeleted());
    vec_ptrs.push_back(new ToBeDeleted());
    vec_ptrs.push_back(new ToBeDeleted());

    std::for_each(vec_ptrs.begin(), vec_ptrs.end(), &boost::checked_delete<ToBeDeleted>);
}
