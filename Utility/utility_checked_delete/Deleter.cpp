#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <boost/checked_delete.hpp>

void Deleter::delete_it(ToBeDeleted* p)
{
    boost::checked_delete(p);
}
