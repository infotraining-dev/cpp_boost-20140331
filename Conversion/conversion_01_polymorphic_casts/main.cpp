#define NDEBUG
#include <iostream>
#include <string>
#include <stdexcept>
#include <exception>
#include <boost/cast.hpp>
#include "Vehicle.hpp"

void travel_on_land(Vehicle* v, int distance)
{
	Car* c = boost::polymorphic_cast<Car*>(v);
	c->drive(distance);
}

void travel_on_sea(Vehicle* v, int distance)
{
	Boat* b = boost::polymorphic_cast<Boat*>(v);
	b->travel(distance);
}

Vehicle* create_vehicle(const std::string& type_id, const std::string& vehicle_id)
{
	if (type_id == "Car")
		return new Car(vehicle_id);
	else if (type_id == "Boat")
		return new Boat(vehicle_id);
	throw std::invalid_argument("Invalid type_id");
}

int main()
{
#   ifdef NDEBUG
		std::cout << "NDEBUG is defined\n";
#   else
		std::cout << "NDEBUG is not defined\n";
#   endif


	// 1 - polymorphic_cast

	Vehicle* vhc = create_vehicle("Car", "KR123444");

	// ok
	travel_on_land(vhc, 10);
	vhc->info();

	// zle
	try
	{
		travel_on_sea(vhc, 20);
	}
	catch(const std::bad_cast& e)
	{
		std::cout << "Niepoprawne rzutowanie: " << e.what() << std::endl;
	}

	delete vhc;

	// 2 - polymorphic_downcast
	vhc = create_vehicle("Car", "PL_WW22344");

    Car* c = boost::polymorphic_downcast<Car*>(vhc);

    c->drive(100);
    c->info();

    delete c;
}
