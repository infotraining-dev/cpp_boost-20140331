#include <iostream>
#include <boost/cast.hpp>


using namespace std;

int main()
{
    int x = 666;

    short sx = boost::numeric_cast<short>(x);

    cout << "sx = " << sx << endl;

    x = 1;

    unsigned int ux = boost::numeric_cast<unsigned int>(x);

    cout << "ux = " << ux << endl;

    cout << "Hello World!" << endl;

    return 0;
}

