#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <typeinfo>
#include <boost/any.hpp>
#include <boost/mem_fn.hpp>
#include <functional>
#include <boost/bind.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/version.hpp>

using namespace std;

template <typename T>
class IsType : public std::unary_function<boost::any, bool>
{
public:
    bool operator()(const boost::any& a) const
    {
        return a.type() == typeid(T);
    }
};

int main()
{
	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji store_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
     * 4 - napisz klasę MapAny
	 */

	// 1
	vector<boost::any> non_empty;

    remove_copy_if(store_anything.begin(), store_anything.end(), back_inserter(non_empty),
                   boost::mem_fn(&boost::any::empty));

	cout << "store_anything.size() = " << store_anything.size() << endl;
	cout << "non_empty.size() = " << non_empty.size() << endl;

	// 2
    int count_int = count_if(store_anything.begin(), store_anything.end(), IsType<int>());

	// TODO
    cout << "store_anything przechowuje " << count_int << " elementow typu int" << endl;


    int count_string = count_if(store_anything.begin(), store_anything.end(), IsType<string>());
	// TODO
    cout << "store_anything przechowuje " << count_string << " elementow typu string" << endl;

	// 3
    list<string> string_items;
	// TODO

    string (*custom_cast)(const boost::any&) = &boost::any_cast<string>;

    boost::transform(store_anything | boost::adaptors::reversed | boost::adaptors::filtered(IsType<string>()),
                     back_inserter(string_items), ptr_fun(custom_cast));

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;

    // 4
    /*
    MapAny m;

    m.insert("id", 1);
    m.insert("name", string("Adam"));
    m.insert("age", 33);

    try
    {
        string id = m.get<string>("id");
        cout << "id = " << id << endl;
        string name = m.get<string>("name");
        cout << "name = " << name << endl;
        string address = m.get<string>("address");
    }
    catch(InvalidValueType& e)
    {
        cout << e.what() << endl;
    }
    catch(KeyNotFound& e)
    {
        cout << e.what() << endl;
    }
    */
}
