#include <iostream>
#include <cmath>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/static_assert.hpp>
#include <boost/fusion/adapted/boost_tuple.hpp>
#include <boost/fusion/adapted.hpp>
#include <boost/fusion/algorithm.hpp>
#include <boost/fusion/include/out.hpp>
#include <boost/fusion/include/pair.hpp>
#include <boost/fusion/container/map.hpp>
#include <boost/fusion/include/at_key.hpp>

using namespace std;
using namespace boost;

template <typename P1, typename P2, int Index>
struct Pythagoras
{
    static double apply(const P1& p1, const P2& p2)
    {
        double d = get<Index-1>(p1) + get<Index-1>(p2);
        return d * d + Pythagoras<P1, P2, Index-1>::apply(p1, p2);
    }
};

template <typename P1, typename P2>
struct Pythagoras<P1, P2, 0>
{
    static double apply(const P1& p1, const P2& p2)
    {
        return 0;
    }
};

template <typename P1, typename P2>
double distance_between_points(const P1& p1, const P2& p2)
{
    BOOST_STATIC_ASSERT(boost::tuples::length<P1>::value == boost::tuples::length<P2>::value);

    double accumulated = Pythagoras<P1, P2, boost::tuples::length<P1>::value>::apply(p1, p2);

    return sqrt(accumulated);
}


// with fusion
struct PythagorasFusion
{
    typedef double result_type;

    template <typename T>
    double operator()(double acc, const T& axis) const
    {
        double d = boost::fusion::at_c<0>(axis) - boost::fusion::at_c<1>(axis);

        return acc + d * d;
    }
};

template <typename P1, typename P2>
double distance_between_points_with_fusion(P1 p1, P2 p2)
{
    //BOOST_STATIC_ASSERT(boost::tuples::length<P1>::value == boost::tuples::length<P2>::value);

    typedef boost::fusion::vector<P1&, P2&> ZipType;

    double accumulated = boost::fusion::fold(boost::fusion::zip_view<ZipType>(ZipType(p1, p2)), 0, PythagorasFusion());

    return sqrt(accumulated);
}

struct Coord
{
    double x;
    double y;
};

BOOST_FUSION_ADAPT_STRUCT(
    Coord,
    (double, x)
    (double, y)
)

int main()
{
    typedef tuple<double, double> Point2D;
    typedef tuple<double, double, double> Point3D;

    Point2D pt1(2.0, 0.0);
    Point2D pt2(0.0, 2.0);

    double result = distance_between_points(pt1, pt2);

    cout << "distance between " << pt1 << " and " << pt2 << " = " << result << endl;

    result = distance_between_points_with_fusion(pt1, pt2);

    cout << "fusion ver: distance between " << pt1 << " and " << pt2 << " = " << result << endl;

    Coord c1 = { 2.0, 3.0 };
    Coord c2 = { 6.0, 9.0 };

    cout << "fusion ver with struct" << distance_between_points_with_fusion(c1, c2) << endl;


    // widoki

    using namespace boost;
    using boost::mpl::_;
    using boost::mpl::not_;
    using boost::is_class;

    typedef boost::fusion::vector<std::string, char, long, bool, double> vector_type;

    vector_type v("a-string", '@', 987654, true, 6.6);
    boost::fusion::filter_view<vector_type const, not_<is_arithmetic<_> > > view(v);
    std::cout << view << std::endl;

    // mapy

    typedef fusion::map<
        fusion::pair<int, char>,
        fusion::pair<double, std::string> >
    map_type;

    map_type m(
        fusion::make_pair<int>('X'),
        fusion::make_pair<double>("Men"));

    std::cout << fusion::at_key<int>(m) << std::endl;
    std::cout << fusion::at_key<double>(m) << std::endl;
}

