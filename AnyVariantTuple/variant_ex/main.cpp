#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>

using namespace std;

class AbsVisitor : public boost::static_visitor<double>
{
public:
    int operator()(int n) const
    {
        return abs(n);
    }

    float operator()(float& fn) const
    {
        return fabs(fn);
    }

    double operator()(std::complex<double>& c) const
    {
        return std::abs(c);
    }
};

int main()
{
    typedef boost::variant<int, float, complex<double> > VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(-7);
    vars.push_back(complex<double>(-1, -1));

    AbsVisitor abs_visitor;
    transform(vars.begin(), vars.end(),
              ostream_iterator<double>(cout, " "),
              boost::apply_visitor(abs_visitor));
}
