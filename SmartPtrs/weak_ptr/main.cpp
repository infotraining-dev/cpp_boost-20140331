#include <iostream>
#include <tr1/memory>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <string>
#include <cstdlib>

class Human
{    
public:
	Human()
	{
		std::cout << "Konstruktor Human()\n";
	}
	
	~Human()
	{
		std::cout << "Desktruktor ~Human()\n";
	}
	
	std::string& Name()
	{
		return name_;
	}
	
	void SetPartner(boost::weak_ptr<Human> partner)
	{
		partner_ = partner;
	}
	
	boost::weak_ptr<Human> GetPartner() const
	{
		return partner_;
	}
private:
	boost::weak_ptr<Human> partner_;
	std::string name_;
};

void memoryOK()
{
	// RC husband == 1
	boost::shared_ptr<Human> husband(new Human);
	
	{
		// RC wife == 1
		boost::shared_ptr<Human> wife(new Human);
	
		// RC wife bez zmian RC == 1
		husband->SetPartner(wife);
	
		// RC husband bez zmian == 1
		wife->SetPartner(husband);
		
		// konwersja do shared_ptr za pomocą funkcji lock()
		boost::shared_ptr<Human> partner( wife->GetPartner().lock() );
		
		// sprawdzenie czy obiekt istnieje
		if (partner)
			std::cout << partner->Name() << std::endl;		
	}  // RC wife spada do zera - obiekt jest kasowany
	
	boost::shared_ptr<Human> partner1( husband->GetPartner().lock()) ;
	
	if (partner1 == NULL)
		std::cout << "Obiekt juz zostal usuniety" << std::endl;
			
	try
	{
		boost::shared_ptr<Human> partner2( husband->GetPartner()) ;
	}
    catch(const boost::bad_weak_ptr& e)
	{
		std::cout << "Obiekt juz zostal usuniety. " << e.what() << std::endl;
	}
}

int main()
{
	memoryOK();
}
