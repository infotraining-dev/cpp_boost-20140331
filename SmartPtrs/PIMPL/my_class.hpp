#ifndef MY_CLASS_HPP
#define MY_CLASS_HPP

#include <boost/scoped_ptr.hpp>

class IMyClass
{
public:
    virtual void do_stuff1() = 0;
    virtual void do_stuff2() = 0;
    virtual ~IMyClass() {}
};

class MyClass : public IMyClass
{
public:
    MyClass(size_t size);

    ~MyClass();

    virtual void do_stuff1();
    void do_stuff2();

private:
    class Impl;

    boost::scoped_ptr<Impl> impl_;
};

#endif
