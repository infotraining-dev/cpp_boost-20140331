#include "my_class.hpp"
#include <vector>

class MyClass::Impl
{
    std::vector<char> vec_;
public:
    Impl(size_t size) : vec_(size)
    {}

    void resize(size_t size)
    {
        vec_.clear();
        vec_.resize(size);
    }
};

MyClass::MyClass(size_t size) : impl_(new Impl(size))
{
}

MyClass::~MyClass()
{
}

void MyClass::do_stuff1()
{
    impl_->resize(255);
}

void MyClass::do_stuff2()
{
    impl_->resize(512);
}
