#include <iostream>
#include <memory>
#include "my_class.hpp"

using namespace std;

int main()
{
    auto_ptr<IMyClass> ptr_myclass(new MyClass(128));

    ptr_myclass->do_stuff1();

    MyClass mc;

    MyClass copy_of_mc = mc;

    cout << "Hello World!" << endl;
    return 0;
}

