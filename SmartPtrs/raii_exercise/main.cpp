#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <memory>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

class FileGuard : boost::noncopyable
{
public:
    explicit FileGuard(FILE* file) : file_(file)
    {
        if (file_ == 0)
            throw std::runtime_error("Blad otwarcia pliku!!!");
    }

    FILE* get() const
    {
        return file_;
    }

    ~FileGuard()
    {
        fclose(file_);
    }
private:
    FILE* file_;
};

// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file(fopen(file_name, "w"));

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

class FileCloser
{
public:
    void operator()(FILE* file)
    {
        std::cout << "FileCloser.operator() called" << std::endl;
        fclose(file);
    }
};

void save_to_file_with_shared_ptr(const char* file_name)
{
    FileCloser fc;
    FILE* c_file = 0;
    std::unique_ptr<FILE, FileCloser> file(c_file, fc);

   if (!file)
       throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    save_to_file_with_shared_ptr("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::string temp;
    std::getline(std::cin, temp);
}
