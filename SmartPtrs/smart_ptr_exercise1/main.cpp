#include <memory>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <vector>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;

class X
{
public:
	// konstruktor
	X(int value = 0)
		: value_(value)
	{
		std::cout << "Konstruktor X(" << value_ << ")\n"; 
	}
	
	// destruktor
	~X() 
	{
		std::cout << "Destruktor ~X(" << value_ << ")\n"; 
	}
	
    int value() const
	{
		return value_;
	}

    void set_value(int value)
	{
        value_ = value;
	}

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
	int value_;
};

void legacy_code(X* ptr)
{
    ptr->set_value(5);
    cout << "legacy_code: new value = " << ptr->value() << endl;
}

auto_ptr<X> factory(int arg) // TODO: poprawa z wykorzystaniem smart_ptr
{
    return auto_ptr<X>(new X(arg));
}

unique_ptr<X> factory_c0x(int arg) // TODO: poprawa z wykorzystaniem smart_ptr
{
    return unique_ptr<X>(new X(arg));
}

boost::shared_ptr<X> shared_factory(int arg)
{
    return boost::make_shared<X>(arg);
}

X* legacy_function(unsigned int size)
{
    X* xarray = new X[size];

    for(unsigned int i = 0; i < size; ++i)
        xarray[i].set_value(i);

    return xarray;
}

void unsafe1()  // TODO: poprawa z wykorzystaniem smart_ptr
{
    boost::scoped_ptr<X> ptrX(factory(4));

	/* kod korzystajacy z ptrX */

    legacy_code(ptrX.get());

    ptrX->unsafe();
}

void unsafe2()
{
    int size = 10;

    unique_ptr<X[]> buffer(legacy_function(size));

    /* kod korzystający z buffer */

    for(int i = 0; i < size; ++i)
        buffer[0].unsafe();
}

unique_ptr<X> setter(unique_ptr<X> arg, int value)
{
    arg->set_value(value);

    return arg;
}

unique_ptr<X> create_x(int value)
{
    unique_ptr<X> local_ptr(new X(value));

    local_ptr->set_value(value * 2);

    return local_ptr;
}

int main() try
{
    {
        unique_ptr<X> aptr_x = create_x(13);

        unique_ptr<X> copy_aptr_x = move(aptr_x);

        vector<unique_ptr<X>> vecx;

        unique_ptr<X> local(new X(34));
        vecx.push_back(unique_ptr<X>(new X(33)));
        vecx.push_back(move(local));

        vecx[0]->set_value(35);

        setter(move(copy_aptr_x), 99);

        if (copy_aptr_x.get() == NULL)
        {
            cout << "copy_aptr_x" << endl;
        }
        else
            copy_aptr_x->set_value(15);

        if (aptr_x.get() == NULL)
        {
            cout << "aptr_x jest pusty" << endl;
        }
        else
            aptr_x->unsafe();

        aptr_x.reset(new X(44));

        X* raw_ptr = aptr_x.release();
        delete raw_ptr;

        boost::shared_ptr<X> sp_x;

        {
            vector<boost::shared_ptr<X> > vec_shared;

            vec_shared.push_back(boost::make_shared<X>(101));
            vec_shared.push_back(boost::make_shared<X>(102));
            vec_shared.push_back(boost::make_shared<X>(103));

            sp_x = vec_shared[1];
        }

        cout << "Po wyjsciu z bloku..." << endl;
    }


    //unsafe1();
    unsafe2();
}
catch(...)
{
    std::cout << "Zlapalem wyjatek!" << std::endl;
}
