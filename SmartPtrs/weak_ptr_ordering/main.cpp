#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/make_shared.hpp>
#include <stdexcept>
#include <set>

using namespace std;

class X
{
public:
    // konstruktor
    X(int value = 0)
        : value_(value)
    {
        std::cout << "Konstruktor X(" << value_ << ")\n";
    }

    // destruktor
    ~X()
    {
        std::cout << "Destruktor ~X(" << value_ << ")\n";
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
    int value_;
};

void compare_weaks(boost::weak_ptr<X> w1, boost::weak_ptr<X> w2, const string& n1, const string& n2)
{
    bool are_equiv = !(w1 < w2) && !(w2 < w1);
    cout << boolalpha << "wp - " << n1 << " eq " << n2 << ": " << are_equiv << endl;
}

void compare_shared(boost::shared_ptr<X> s1, boost::shared_ptr<X> s2, const string& n1, const string& n2)
{
    bool are_equiv = !(s1 < s2) && !(s2 < s1);
    cout << boolalpha << "sp - " << n1 << " eq " << n2 << ": " << are_equiv << endl;
}

int main()
{
    boost::weak_ptr<X> w1;
    boost::weak_ptr<X> w2;

    compare_weaks(w1, w2, "w1_null", "w2_null");

    {
        boost::shared_ptr<X> ptrx1;

        {
            boost::shared_ptr<X> ptrx2;

            compare_shared(ptrx1, ptrx2, "ptrx1_null", "sptrx2_null");
            compare_weaks(ptrx1, ptrx2, "ptrx1_null", "ptrx2_null");

            cout << "\n";

            ptrx1 = boost::make_shared<X>(1);
            ptrx2 = boost::make_shared<X>(2);

            compare_shared(ptrx1, ptrx2, "ptrx1", "ptrx2");
            compare_weaks(ptrx1, ptrx2, "ptrx1", "ptrx2");

            cout << "\n";

            w1 = ptrx1;
            w2 = ptrx2;

            compare_weaks(w1, w2, "w1_ptrx1", "w2_ptrx2");

            cout << "\n";

            w1 = ptrx2;

            compare_weaks(w1, w2, "w1_ptrx2", "w2_ptrx2");
        }
        cout << "\nAfter deleting ptrx2:\n";
        compare_weaks(w1, w2, "w1_ptrx2_deleted", "w2_ptrx2_deleted");

        boost::weak_ptr<X> w3 = w2;

        compare_weaks(w2, w3, "w2_ptrx2_deleted", "w3");
    }
    cout << "\nAfter deleting ptrx1:\n";

    compare_weaks(w1, w2, "w1_ptrx1_deleted", "w2_ptrx2_deleted");
}

