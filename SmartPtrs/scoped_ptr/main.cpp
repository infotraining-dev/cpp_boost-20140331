#include <iostream>
#include <stdexcept>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <cstdlib>

using namespace std;

class ManagedObject
{
	static unsigned int counter_;
public:
	ManagedObject() : obj_(++counter_)
	{	
		cout << "Konstruktor ManagedObject(" << obj_ << ")" << endl;
	}
	
	~ManagedObject()
	{
		cout << "Destruktor ~ManagedObject( " << obj_ << ")" << endl;
	}
	
	int& obj()
	{
		return obj_;
	}

	void do_stuff() const
	{
		cout << "ManagedObject.do_stuff()" << endl;
	}
	
private:
	int obj_;
};

unsigned int ManagedObject::counter_ = 0;

int main()
{
	// prezentacja dzialania scoped_ptr
	try
	{
		boost::scoped_ptr<ManagedObject> scManaged(new ManagedObject());
		scManaged->do_stuff();
		scManaged.reset();

		boost::scoped_ptr<ManagedObject> scPtrObject(new ManagedObject());
		
		scPtrObject->do_stuff();
		
		throw std::out_of_range("Błąd");
		
		scPtrObject->do_stuff();
	}
	catch (...)
	{
		cout << "Obsługa wyjątku." << endl;
	}

	cout << "--------------------------------------\n";

	try
	{
		// prezenatacja dzialania scoped_array
		boost::scoped_array<ManagedObject> scArr(new ManagedObject[10]);

		for(int i = 0; i < 10; ++i)
			scArr[i].do_stuff();

		throw std::runtime_error("Runtime Exception");
	}
	catch (...)
	{
		cout << "Obsluga wyjatku" << endl;
	}
}

