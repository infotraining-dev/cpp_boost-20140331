#include <iostream>
#include <fstream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/lexical_cast.hpp>
#include "active_object.hpp"

using namespace std;

class LoggerWithMutex
{
    ofstream fout_;
    boost::mutex mtx_;
public:
    LoggerWithMutex(const string& file_name)
    {
        fout_.open(file_name);
    }

    ~LoggerWithMutex()
    {
        fout_.close();
    }

    void log(const string& message)
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        fout_ << message << endl;
    }
};

class LoggerWithActiveObject
{
    ofstream fout_;
    ActiveObject ao_;
public:
    LoggerWithActiveObject(const string& file_name)
    {
        fout_.open(file_name);
    }

    ~LoggerWithActiveObject()
    {
        fout_.close();
    }

    void log(const string& message)
    {
        ao_.send(boost::bind(&LoggerWithActiveObject::do_log, this, message));
    }

private:
    void do_log(const string& message)
    {
        fout_ << message << endl;
    }
};

typedef LoggerWithActiveObject Logger;

void run(Logger& logger, int id)
{
    for(int i = 0; i < 1000; ++i)
        logger.log("Log#" + boost::lexical_cast<string>(id) + " - Event#" + boost::lexical_cast<string>(i));
}

int main()
{
    /*
     * Napisz klase Logger, ktora jest thread-safe
     */

    Logger log("data.log");

    vector<boost::thread> thds;

    for (int i = 0; i < 20; ++i)
        thds.emplace_back(&run, boost::ref(log), i);

    for(auto& t : thds)
        t.join();
}
