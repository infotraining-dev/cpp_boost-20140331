#include <iostream>
#include <string>
#include <iterator>
#include <boost/function.hpp>
#include <boost/bind.hpp>
//#include <boost/lambda/lambda.hpp>

using namespace std;
//using namespace boost::lambda;

bool some_function(int i, double d)
{
	return i > d;
}

class SomeFunctor : public std::binary_function<string, string, bool>
{
public:
	bool operator()(const std::string& s1, const std::string& s2) const
	{
		return s1.size() < s2.size();
	}
};

class ArrayInitializer
{
	int current_;
public:
	ArrayInitializer(int init_value = 0) : current_(0)
	{
	}

	void init_array(int arr[], size_t size)
	{
		for(size_t i = 0; i < size; ++i)
			arr[i] = current_++;
	}
};

int main()
{
	try
	{
		// niezainicjowany obiekt function
		boost::function<bool (int, double)> f0;
		f0(2, 3.14);
	}
	catch(const boost::bad_function_call& e)
	{
		cout << "Z�apa�em wyj�tek: " << e.what() << endl;
	}

	cout << "--------------------------------\n";

	// 1 - adres funkcji
	boost::function<bool (int, double)> f1;

	// przypisanie adresu funkcji
	f1 = &some_function;

	cout.setf(ios::boolalpha);
	cout << "f1(1, 3.14) = " << f1(1, 3.14) << endl;

	cout << "--------------------------------\n";

	// 2 - adres metody

	boost::function<void (ArrayInitializer*, int[], size_t)> f2;

	f2 = &ArrayInitializer::init_array;

	int numbers[10];
	ArrayInitializer ai;

	if (f2)
		f2(&ai, numbers, 10);
	else
		cout << "Obiekt f2 jest pusty." << endl;

	cout << "numbers: ";
	copy(numbers, numbers+10, ostream_iterator<int>(cout, " "));
	cout << "\n";

	if (f2)
			f2(&ai, numbers, 10);
		else
			cout << "Obiekt f2 jest pusty." << endl;

	cout << "numbers: ";
		copy(numbers, numbers+10, ostream_iterator<int>(cout, " "));
		cout << "\n";

	cout << "--------------------------------\n";

	// 3 - obiekt funkcyjny

	boost::function<bool (string, string)> f3;

	f3 = SomeFunctor();

	cout << "f3(string(\"Dyzma\"), string(\"Dymski\")) = " << f3("Dyzma", "Dymski") << endl;

	cout << "--------------------------------\n";

    // 4 - obiekt wiazania bind

	boost::function<bool (string)> f4 = boost::bind(SomeFunctor(), _1, "Dymski");

	cout << "f3(\"Dyzma\") = " << f4("Dyzma") << endl;

	cout << "--------------------------------\n";


// 5 - wyra�enie lambda
//	boost::function<void (int, int)> f5;
//
//	int x = 5;
//	int y = 10;
//
//	f5 = cout << constant("x = ") << _1 << "; y = " << _2 << "\n";
//
//	f5(x, y);

    boost::function<bool (int)> is_even = [](int x) { return x % 2 == 0; };

    cout << "is_even = " << is_even(2) << endl;
}
