#include <iostream>
#include <queue>
#include <boost/function.hpp>
#include <boost/bind.hpp>

using namespace std;

class Worker
{
    typedef boost::function<void ()> CommandType;
    queue<CommandType> tasks_;
public:
    void register_task(CommandType cmd)
    {
        tasks_.push(cmd);
    }

    void run()
    {
        while(!tasks_.empty())
        {
            CommandType cmd = tasks_.front();
            cmd();
            tasks_.pop();
        }
    }
};

class Printer
{
public:
    void print(const string& text)
    {
        cout << "Print:" << text << endl;
    }

    void off()
    {
        cout << "Printer.off\n";
    }

    void on()
    {
        cout << "Printer.on\n";
    }
};

int main()
{
    Printer prn;

    Worker worker;

    worker.register_task(boost::bind(&Printer::on, &prn));
    worker.register_task(boost::bind(&Printer::print, &prn, "Tekst"));
    worker.register_task(boost::bind(&Printer::off, &prn));

    //...

    worker.run();

    return 0;
}

