#include <iostream>
#include <algorithm>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <vector>
#include <boost/assign.hpp>
#include <boost/algorithm/cxx11/copy_if.hpp>
#include <list>
#include <iterator>
#include <boost/tuple/tuple_io.hpp>

void foo(int x, int y)
{
	std::cout << "x: " << x << " y: " << y << std::endl;
}

void nine_arguments(int i1, int i2, int i3, int i4, int i5,
		int i6, int i7, int i8, int i9)
{
	std::cout << i1 << i2 << i3 << i4 << i5 << i6 << i7 << i8 << i9 << std::endl;
}

class Workaround
{

};

std::ostream& operator<<(std::ostream& out, Workaround w)
{
    return out;
}

class Worker
{
	int id_;
    bool ok_;
public:
    explicit Worker(int id = 0) : id_(id), ok_(true)
	{
	}

	void print(const std::string& prefix) const
	{
        std::cout << prefix << id_
                  << std::boolalpha << " - " << ok_ << std::endl;
	}

    bool is_valid() const
    {
        return ok_;
    }

    void break_it()
    {
        ok_ = false;
    }
};

class Add //: public std::binary_function<int, int, int>
{
public:
    int operator()(int x, int y) const
    {
        return x + y;
    }
};

void change_args(int& x, Worker& w)
{
    x++;

    w.break_it();
}

int main()
{
    boost::function<void (int)> f1 = boost::bind(&foo, 1, _1);

    f1(2);  // foo(1, 2)

	// proste wiazanie
	boost::bind(&foo, _1, _2)(1, 2);
	boost::bind(&foo, _2, _1)(1, 2);

	std::cout << "------------\n";

	// zamiast std::bind1st lub std::bind2nd
    std::bind1st(std::ptr_fun(&foo), 5)(1);
	// to samo, ale z bind
	boost::bind(&foo, 5, _1)(1);

	std::cout << "------------\n";

	std::bind2nd(std::ptr_fun(&foo), 5)(1);
	boost::bind(&foo, _1, 5)(1);

	std::cout << "------------\n";

	// bind moze wiazac funkcje przyjmujace do 9 argumentow
	int i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5, i6 = 6, i7 = 7, i8 = 8, i9 = 9;
	
	(boost::bind(&nine_arguments, _9, _2, _1, _3, _4, _5, _6, _7, _8))(i1, i2, i3, i4, i5, i6, i7, i8, i9);

	std::cout << "------------\n";

    // wiazanie wywolania metody z obiektem
	Worker w(13);

	boost::bind(&Worker::print, _1, "id = ")(w);

    using namespace boost::assign;
    std::vector<Worker> vec_workers = list_of(Worker(4))(Worker(5));
    vec_workers += Worker(1), Worker(2), Worker(3);

    vec_workers[0].break_it();
    vec_workers[3].break_it();

    std::for_each(vec_workers.begin(), vec_workers.end(),
                  boost::bind(&Worker::print, _1, "Worker: "));

    std::list<Worker> valid_workers;

    boost::algorithm::copy_if(vec_workers,
                              std::back_inserter(valid_workers),
                              boost::mem_fn(&Worker::is_valid));

    valid_workers.clear();

    std::remove_copy_if(vec_workers.begin(), vec_workers.end(),
                              std::back_inserter(valid_workers),
                              !boost::bind(&Worker::is_valid, _1));

    std::cout << "\nValid workers:\n";

    std::for_each(valid_workers.begin(), valid_workers.end(),
                  boost::bind(&Worker::print, _1, "Worker: "));

    int x = 10;

    auto f2 = boost::bind(&change_args, boost::ref(x), boost::ref(w));

    f2();

    std::cout << "x = " << x << " status: " << w.is_valid() << std::endl;

    Worker w2(13);

    auto f3 = boost::bind(&Worker::break_it, &w2);

    f3();

    w2.print("w2: ");

    Add add;

    auto f4 = boost::bind<int>(add, _1, 10);

    std::cout << f4(5) << std::endl;
}
