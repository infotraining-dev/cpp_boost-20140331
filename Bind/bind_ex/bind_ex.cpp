#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <functional>
#include <boost/algorithm/cxx11/copy_if.hpp>
#include <boost/bind.hpp>


template <typename InIt, typename F>
void for_each(InIt start, InIt end, F f)
{
    while(start != end)
        f(*start++);
    }

using namespace std;

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    using namespace boost::algorithm;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::salary, _1) > 3000.0);

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::age, _1) < 30);

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";

    boost::function<int (Person)> f1 = boost::bind(&Person::age, _1);
    auto f2 = boost::bind(&Person::age, _2);

    sort(employees.begin(), employees.end(),
         f1 > f2);

    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::gender, _1) == Female);

    // ilość osob zarabiajacych powyżej średniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
//    double avg_salary
//       = accumulate(employees.begin(), employees.end(), 0.0,
//                    boost::bind(plus<double>(),
//                       _1,
//                       boost::bind(&Person::salary, _2))) / employees.size();

    double avg_salary
       = accumulate(employees.begin(), employees.end(), 0.0,
                    [](double arg1, const Person& p)
                    { return arg1 + p.salary(); }) / employees.size();

    cout << count_if(employees.begin(), employees.end(),
                     boost::bind(&Person::salary, _1) > avg_salary) << endl;

    cout << "avg_salary: " << avg_salary << endl;
}
